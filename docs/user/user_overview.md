# Overview

Comfor is an explicit non linear finite element software for composite modelling modelling. It is suitable for dynamic transient response and some quasi static problems by using dynamic relaxation.

The different materials, finite elements, contact laws come from confirmed and recent research works. 

This section will allow to establish the basic notions for the construction of a numerical model for Comfor, the simulation and its analysis. 

1. [Pre-processing](user_preprocessing.md)
    - Control 
    - Materials
    - Boundary conditions 
    - Amplitudes
    - Loads
    - Nodes
    - Elements
    - Contact
    - Trackers
2. [Analysis](user_analysis.md)
    - Starting a new analysis
    - Runtime information
    - Errors and bugs
3. [Post-processing](user_postprocessing.md)  
    - Load the files 
    - Play the animation
    - Applying filters
    - Ressources