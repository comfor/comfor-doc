# Downloads

## Version

### Last version `V0.8.0`:

  - Windows 7/10
    - <a href="../../assets/bin/v0_8_0/Release-windows-vs2019/COMFOR_V0_8_0_VS2019.exe" download>Download Comfor V0.8.0 for Windows (Visual Studio 2019)</a>
  - Ubuntu 18.04
    - <a href="../../assets/bin/v0_8_0/Release-ubuntu-18.04-gcc/COMFOR_V0_8_0_GCC7_5" download>Download Comfor V0.8.0 for Ubuntu 18.04 (GCC 7.5.0)</a>
  - Ubuntu 20.04
    - <a href="../../assets/bin/v0_8_0/Release-ubuntu-20.04-gcc/COMFOR_V0_8_0_GCC9_3" download>Download Comfor V0.8.0 for Ubuntu 20.04 (GCC 9.3.0)</a>
  - Rocky Linux 8
    - <a href="../../assets/bin/v0_8_0/Release-rocky-linux-8-gcc/COMFOR_V0_8_0_GCC9_2" download>Download Comfor V0.8.0 for Rocky Linux 8 (GCC 9.2.1)</a>
  - macOS Monterey
    - <a href="../../assets/bin/v0_8_0/Release-macos-sonoma-clang/COMFOR_V0_8_0_CLANG15" download>Download Comfor V0.8.0 for macOS Sonoma (Clang 15.0.0)</a>

### Version `V0.7.4`:

  - Windows 7/10
    - <a href="../../assets/bin/v0_7_4/COMFOR_V0_7_4_VS2019.exe" download>Download Comfor V0.7.4 for Windows (Visual Studio 2019)</a>
  - Ubuntu 18.04
    - <a href="../../assets/bin/v0_7_4/COMFOR_V0_7_4_GCC7_5" download>Download Comfor V0.7.4 for Ubuntu 18.04 (GCC 7.5.0)</a>
  - Ubuntu 20.04
    - <a href="../../assets/bin/v0_7_4/COMFOR_V0_7_4_GCC9_3" download>Download Comfor V0.7.4 for Ubuntu 20.04 (GCC 9.3.0)</a>
  - macOS Monterey
    - <a href="../../assets/bin/v0_7_4/COMFOR_V0_7_4_CLANG13" download>Download Comfor V0.7.4 for macOS (Clang 13.0.0)</a>

### Version `V0.5.1`:

  - Windows
    - <a href="../../assets/bin/COMFOR_V0_5_1.exe" download>Download Comfor V0.5.1 for Windows (Visual Studio 2019)<</a>
  - Ubuntu
    - <a href="../../assets/bin/COMFOR_V0_5_0_GCC_10_2_0" download>Download Comfor V0.5.0 for Ubuntu 20.04 (GCC 10.2.0)</a>
  - macOS Big Sur
    - <a href="../../assets/bin/COMFOR_V0_5_1_OSX_clang11" download>Download Comfor V0.5.1 for macOS (Clang 11.0.3)</a>

!!! note
    For Windows : Install the corresponding [Visual C++ redistributable packages for Visual Studio](https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0)
    
## Examples

Some examples to run with comfor:

- <a href="../../assets/examples/examples.zip" download>Click to Download the examples</a>
