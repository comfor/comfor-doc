## Extension list

- [C++ Microsoft extensions](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools)
- [Visul Studio Intellicode](https://marketplace.visualstudio.com/items?itemName=VisualStudioExptTeam.vscodeintellicode)
- [Cmake Tools](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cmake-tools)
- [Cmake Langage support](https://marketplace.visualstudio.com/items?itemName=twxs.cmake)
- [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)
- [Bracket Pair Colorizer 2](https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2)
- [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [Doxygen Documentation Generator](https://marketplace.visualstudio.com/items?itemName=cschlosser.doxdocgen)
