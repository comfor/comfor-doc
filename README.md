# Comfor Doc

Public documentation of the Comfor project

# Build

Install requirement using `pip`

```sh
python3 -m venv .venv
source .venv/bin/activate
python3 -m pip install -r requirements
```

and build the site:

```sh
mkdocs build --strict --verbose
```

# Dev

Live server

```sh
mkdocs serve
```

See [mkdocs documentation](https://www.mkdocs.org/getting-started/).

# License

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
    <img alt="Creative Commons License"
    style="border-width: 0"
    src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
    <br/>
    <span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">
    Comfor Documentation
    </span>
    by
    <a xmlns:cc="http://creativecommons.org/ns#"
    href="https://egm_foss.gitlab.io/about_me/"
    property="cc:attributionName"
    rel="cc:attributionURL"
    >Eduardo Guzman
    </a>
    is licensed under a
    <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
    Creative Commons Attribution-ShareAlike 4.0 International License
    </a>.
    <br/>Based on a work at
    <a xmlns:dct="http://purl.org/dc/terms/"
       href="https://gitlab.com/comfor/comfor-doc"
       rel="dct:source">
    https://gitlab.com/comfor/comfor-doc
</a>.
